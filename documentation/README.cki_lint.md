---
title: cki_lint.sh
description: >-
    Wrapper to run Python unit tests and a set of common linters across all of
    CKI
---

Runs `flake8`, `pydocstyle`, `isort --check` and `pytest` under `coverage`. The
script takes care to install the needed dependencies via pip beforehand. For
`pylint` and `pytest`, only the packages specified as parameters will be checked.

Performs linting and link checking for all `README.*.md` files. It uses the
markdownlint tool to check for markdown errors and a `cki_check_links.sh`
script to check for broken links.

The experimental `--fix` parameter can be used to automatically fix some of the
detected issues. At the moment, this will invoke `autopep8` and `isort` without
`--check`.

For correct editor integration of linters/fixers, the following parameters must
be set:

- `flake8`: `--max-line-length 100`
- `autopep8`: `--max-line-length 100`
- `isort`: `--line-length 100 --force-single-line-imports --force-sort-within-sections`

When run in a GitLab CI/CD pipeline for a merge request, the script tries to
determine the previous code coverage and will fail if code coverage decreased.
This can be skipped by adding `[skip coverage check]` with an explanation to
the merge request description.

## Environment variables

| Environment variable    | Description                                                       |
|-------------------------|-------------------------------------------------------------------|
| `CKI_COVERAGE_ENABLED`  | `false` if coverage calculation should be disabled                |
| `CKI_DISABLED_LINTERS`  | linters to disable or `all`                                       |
| `CKI_PYLINT_ARGS`       | Additional arguments for pylint, e.g. to disable certain warnings |
| `CKI_PYTEST_ARGS`       | Additional arguments for pytest, e.g. custom selection of tests   |
| `CKI_PYTEST_IGNORELIST` | Directories to ignore for test discovery                          |

### Running Python tests and linting

To run both Python tests and linting, execute the following command:

```shell
tox
```

To run only a custom selection of the tests, use:

```shell
TOX_OVERRIDE=testenv.passenv+=CKI_PYTEST_ARGS \
    CKI_PYTEST_ARGS="tests/file1.py[::classname[::test_name]] [tests/file2.py...]" \
    tox
```

For example, the following command runs `test_get_pipeline` in
`tests/test_gitlab.py` and all the tests in `tests/test_cronjob.py`:

```shell
TOX_OVERRIDE=testenv.passenv+=CKI_PYTEST_ARGS \
    CKI_PYTEST_ARGS="tests/test_gitlab.py::TestGitLabParseURL::test_get_pipeline tests/test_cronjob.py" \
    tox
```

## Disable coverage report

An additional useful environment variable is `CKI_COVERAGE_ENABLED`,
which can be used to disable the coverage report, leaving a cleaner output from tests.

```shell
TOX_OVERRIDE=testenv.passenv+=CKI_COVERAGE_ENABLED \
    CKI_COVERAGE_ENABLED="false" \
    tox
```

## Disabling linters

To skip certain linters, specify them in the `CKI_DISABLED_LINTERS` variable as a
space-separated list. For example:

```shell
TOX_OVERRIDE=testenv.passenv+=CKI_DISABLED_LINTERS \
    CKI_DISABLED_LINTERS="markdownlint pylint" \
    tox
```

The above command skips the `markdownlint` and `pylint` linters when running
tox.

To skip all linters, set `CKI_DISABLED_LINTERS` to "all":

```shell
TOX_OVERRIDE=testenv.passenv+=CKI_DISABLED_LINTERS \
    CKI_DISABLED_LINTERS="all" \
    tox
```

You can also provide both `CKI_PYTEST_ARGS` and `CKI_DISABLED_LINTERS` if you
wish to skip linters and run specific tests. For example:

```shell
TOX_OVERRIDE=testenv.passenv+=CKI_PYTEST_ARGS,CKI_DISABLED_LINTERS \
    CKI_PYTEST_ARGS="tests/test_gitlab.py::TestGitLabParseURL::test_get_pipeline tests/test_cronjob.py" \
    CKI_DISABLED_LINTERS="all" \
    tox
```

The above command skips all linters and runs the `test_get_pipeline` test in
`tests/test_gitlab.py`, as well as all tests in `tests/test_cronjob.py`.
