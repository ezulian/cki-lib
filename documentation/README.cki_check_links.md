---
title: cki_check_links.sh
description: >-
    Link checker for Markdown files that uses the "urlchecker" tool to perform
    the link checks
---

This Bash script checks the links in the Markdown file given on the command
line. It uses the "urlchecker" tool to perform the link checks.

The script reads URLs to exclude from files that match the pattern
`check-links-*.txt`.
