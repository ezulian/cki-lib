---
title: cki_lib.owners
linkTitle: owners
description: >-
    Parse the kernel owners.yaml file and print the responsible
    subsystems for kernel source files
---

The [owners.yaml] file can be found in the [CentOS Stream kernel documentation repository].

```bash
usage: python3 -m cki_lib.owners [-h]
    [--owners-yaml OWNERS_YAML]
    [--owners-yaml-path OWNERS_YAML_PATH]
    [--output {json,text}]
    --
    [files ...]

Print responsible subsystems for kernel source files

positional arguments:
  files                 kernel source files

options:
  -h, --help            show this help message and exit
  --owners-yaml OWNERS_YAML
                        contents of owners.yaml (default: env[OWNERS_YAML])
  --owners-yaml-path OWNERS_YAML_PATH
                        path of owners.yaml (default: env[OWNERS_YAML_PATH])

  --output {json,text}  output format (default: text)
```

## Configuration via environment variables

| Name               | Type | Secret | Required | Description                                                                       |
|--------------------|------|--------|----------|-----------------------------------------------------------------------------------|
| `OWNERS_YAML`      | yaml | no     | no       | Contents of owners.yaml in YAML. If not present, falls back to `OWNERS_YAML_PATH` |
| `OWNERS_YAML_PATH` | path | no     | no       | Path to owners.yaml                                                               |
| `SENTRY_DSN`       | url  | yes    | no       | Sentry DSN                                                                        |

[owners.yaml]: https://gitlab.com/redhat/centos-stream/src/kernel/documentation/-/blob/main/info/owners.yaml
[CentOS Stream kernel documentation repository]: https://gitlab.com/redhat/centos-stream/src/kernel/documentation
