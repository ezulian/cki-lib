#!/usr/bin/env bash

set -Eeuo pipefail
# no shopt -s inherit_errexit as this needs to run on the ancient Bash in RHEL7
# instead, has 'set -e; ' below as recommended by shellcheck

# pass the packages to test (without tests directory) as args to the script

# shellcheck source-path=SCRIPTDIR
. cki_utils.sh

function did_coverage_decrease() {
    local pipelines_json pipeline_id pipeline_json old_coverage new_coverage=${1}
    shift
    echo -n "  Getting last successful pipeline... "
    if ! [[ -v CI_MERGE_REQUEST_PROJECT_ID ]]; then
        cki_echo_yellow "skipped because not in CI MR job"
        return 1
    fi
    if [[ -v CI_MERGE_REQUEST_TARGET_BRANCH_SHA ]]; then
        # for merged result pipelines, the change of the MR is introduced by
        # the merge commit, so take the coverage of the last target branch
        # commit right before the merge commit as the reference
        # shellcheck disable=SC2154
        pipelines_json=$(curl -Ss "${CI_API_V4_URL}/projects/${CI_MERGE_REQUEST_PROJECT_ID}/pipelines?sha=${CI_MERGE_REQUEST_TARGET_BRANCH_SHA}&status=success")
    else
        # shellcheck disable=SC2154
        pipelines_json=$(curl -Ss "${CI_API_V4_URL}/projects/${CI_MERGE_REQUEST_PROJECT_ID}/pipelines?ref=${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}&status=success")
    fi
    pipelines_length=$(jq '. | length' <<< "${pipelines_json}")
    if (( pipelines_length == 0 )); then
        cki_echo_yellow "skipped because no successful pipeline found"
        return 1
    fi
    pipeline_id=$(jq '.[0].id' <<< "${pipelines_json}")
    cki_echo_green " ${pipeline_id}"
    echo -n "  Getting old coverage... "
    pipeline_json=$(curl -Ss "${CI_API_V4_URL}/projects/${CI_MERGE_REQUEST_PROJECT_ID}/pipelines/${pipeline_id}")
    old_coverage=$(jq -r '.coverage // "0.00"' <<< "${pipeline_json}")
    # round up with a precision of two decimals, eg 65.9518 -> 65.96
    if python -c "import math; exit(math.ceil(${new_coverage} * 100) / 100 < ${old_coverage})"; then
        cki_echo_green "${old_coverage}%"
        return 1
    else
        if [[ -v CI_MERGE_REQUEST_TARGET_BRANCH_SHA ]]; then
            # for merged result pipelines, also check whether lines of code
            # increased, and only fail the coverage check in that case
            if git diff --numstat "${CI_MERGE_REQUEST_TARGET_BRANCH_SHA}..HEAD" -- "$@" | \
                awk '{ added += $1; removed += $2 } END { exit added - removed > 0 }'; then
                cki_echo_yellow "${old_coverage}% higher than new ${new_coverage}%, but line count did not increase"
                return 1
            else
                cki_echo_red "${old_coverage}% higher than new ${new_coverage}%, and line count increased"
                return 0
            fi
        else
            cki_echo_red "${old_coverage}% higher than new ${new_coverage}%"
            return 0
        fi
    fi
}

cki_say "preparing"

PIP_INSTALL=(python3 -m pip install)
python_type=$(type -P python3)
if [[ ${python_type} = /usr* ]]; then
    PIP_INSTALL+=(--user)
fi
if [[ -f constraints.txt ]]; then
    PIP_INSTALL+=(--constraint constraints.txt)
fi

# if cki-lib requires overriding, update it and restart the script.
if [[ -n "${cki_lib_pip_url:-}" ]] && ! [[ -v SKIP_FURTHER_CKI_LINT_EXEC ]]; then
    cki_echo_yellow "Using cki-lib override: ${cki_lib_pip_url}"
    python3 -m pip uninstall -y "cki-lib"
    echo "Running ${PIP_INSTALL[*]} ${cki_lib_pip_url}"
    "${PIP_INSTALL[@]}" "${cki_lib_pip_url}"
    cki_echo_yellow "Re-executing cki_lint.sh from updated cki-lib"
    SKIP_FURTHER_CKI_LINT_EXEC=true exec cki_lint.sh "$@"
fi

PACKAGES=(
    flake8
    autopep8
    pydocstyle
    "isort[colors]"
    pylint
    pytest
    pytest-subtests
    coverage
)

echo "Running ${PIP_INSTALL[*]} ${PACKAGES[*]}"
"${PIP_INSTALL[@]}" "${PACKAGES[@]}"

overrides_str=$(compgen -A variable | grep '_pip_url$' || true)
readarray -t overrides <<< "${overrides_str}"
for override in "${overrides[@]}"; do
    if [[ -z ${override} ]] || [[ ${!override} != git+https://* ]]; then
        continue
    fi
    pip_url=${!override}
    package_name=$(set -e; cki_git_clean_url "${pip_url%@*}")
    package_name=${package_name%.git/}
    package_name=${package_name##*/}

    # cki-lib was already handled
    if [[ ${package_name} = cki-lib ]]; then
        continue
    fi

    cki_echo_yellow "Found ${package_name} override: ${pip_url}"
    python3 -m pip uninstall -y "${package_name}"
    "${PIP_INSTALL[@]}" "${pip_url}"
done

MAX_LINE_LENGTH=100
if [[ -f setup.cfg ]] && grep -q 'max-line-length=' setup.cfg; then
    MAX_LINE_LENGTH=$(sed -n s/max-line-length=//p setup.cfg | head -n 1)
    cki_echo_yellow "Overriding maximum line length with ${MAX_LINE_LENGTH}"
fi

_isort=(
    isort
    --color
    --force-single-line-imports
    --force-sort-within-sections
    --line-length "${MAX_LINE_LENGTH}"
    --skip-glob '.*'
    --skip-glob '**/migrations'
)

cki_say "linting"

FAILED=()

# CKI_DISABLED_LINTERS: space separated list of linters to skip.
# The value `all` will skip all linters.
read -ra disabled_linters <<< "${CKI_DISABLED_LINTERS:-}"

function run_flake8() {
    cki_echo_yellow "Running flake8"
    if ! flake8 --max-line-length "${MAX_LINE_LENGTH}" --exclude ".*,migrations" .; then
        FAILED+=(flake8)
        cki_echo_red "  Run cki_lint.sh --fix to run autopep8"
    fi
}

function run_pydocstyle() {
    cki_echo_yellow "Running pydocstyle"
    if ! pydocstyle --match-dir='^(?!(\.|migrations)).*'; then
        FAILED+=(pydocstyle)
    fi
}

function run_isort() {
    cki_echo_yellow "Running isort"
    if ! "${_isort[@]}" "$@" .; then
        FAILED+=(isort)
        if [[ -z "$*" ]]; then
            cki_echo_red "  Run cki_lint.sh --fix to update import order"
        fi
    fi
}

function run_mypy() {
    # If mypy is installed (eg. via the dev extra), run mypy check
    if type mypy > /dev/null 2>&1; then
        cki_echo_yellow "Running mypy"
        if  ! mypy --strict; then
            FAILED+=(mypy)
        fi
    fi
}

function run_markdownlint() {
    # Check and lint README.*.md files
    cki_echo_yellow "Running markdownlint and cki_check_links.sh"

    # Find all README files
    while read -r -d $'\0' file
    do
        # Lint Markdown
        if command -v markdownlint &> /dev/null; then
            # Config for the markdownlint
            markdownlint_config='{
            "line-length": {
                "line_length": 100,
                "code_blocks": false,
                "tables": false
            },
            "no-trailing-punctuation": {
                "punctuation": ".,;:"
            }
            }'
            markdown_config_file="${file%.*}_markdown_config.json"
            echo "${markdownlint_config}" > "${markdown_config_file}"
            markdown_errors_file="${file%.*}_markdown_errors.txt"
            markdownlint --config "${markdown_config_file}" "${file}" > "${markdown_errors_file}" || FAILED+=("markdownlint '${file}'")

            # Check if there are any errors in markdown or links files
            if [[ -s "${markdown_errors_file}" ]]; then
                cki_echo_red "Errors found in ${markdown_errors_file}:"
                cat "${markdown_errors_file}"
            else
                cki_echo_green "No errors found in ${markdown_errors_file}"
            fi

            # Remove *markdown_errors.txt files
            rm "${markdown_errors_file}"

            # Remove markdown_config_file
            rm "${markdown_config_file}"
        else
            cki_echo_yellow "Markdownlint not found. Skipping markdown linting."
        fi

        # Check links in Markdown
        if command -v urlchecker &> /dev/null; then
            cki_check_links.sh "${file}" || FAILED+=("cki_check_links.sh '${file}'")
        else
            cki_echo_yellow "urlchecker not found. Skipping checking links."
        fi

    done < <(find . -name "README*.md" -type f \
                    -not -path "./.tox/*" \
                    -not -path "./.direnv/*" \
                    -not -path "./.pytest_cache/*" \
                    -print0 || true)
}

function run_pylint() {
    # Only run pylint and coverage for the specified packages
    cki_echo_yellow "Running pylint"
    pylint_args=(
        --max-line-length "${MAX_LINE_LENGTH}"
        --load-plugins pylint.extensions.no_self_use
        --disable "broad-except,broad-exception-raised"
    )
    if [[ -f setup.cfg ]]; then
        pylint_args+=(--rcfile setup.cfg)
    fi
    if ! pylint "${pylint_args[@]}" "$@"; then
        FAILED+=(pylint)
    fi
}

function run_pytest() {
    if [[ -f tests/__init__.py ]]; then
        cki_say "testing"
        cki_echo_yellow "Running pytests under coverage"
        if ! coverage run --source "$(IFS=,; echo "$*")" --branch --parallel-mode -m pytest "${pytest_args[@]}"; then
            FAILED+=(pytest)
        fi
        coverage combine --quiet

        cki_echo_yellow "Generating coverage reports"
        # shellcheck disable=SC2154,SC2310
        if ! cki_is_true "${CKI_COVERAGE_ENABLED:-true}"; then
            cki_echo_yellow "  skipped because CKI_COVERAGE_ENABLED is false"
            return 0
        fi
        coverage report -m || true
        coverage html -d coverage/ || true
        coverage xml -o coverage/coverage.xml || true

        cki_echo_yellow "Total coverage and trend"
        new_coverage=$(coverage json -o - | jq .totals.percent_covered)
        echo "  COVERAGE: ${new_coverage}%"

        did_coverage_decrease "${new_coverage}" "$@" &
        wait $! && coverage_decreased=1 || coverage_decreased=0
        if [[ ${coverage_decreased} = 1 ]]; then
            # shellcheck disable=SC2154
            if [[ "${CI_MERGE_REQUEST_DESCRIPTION}" != *"[skip coverage check]"* ]]; then
                FAILED+=(coverage)
                cki_echo_red "  Include [skip coverage check] in the merge request description to skip this check"
            fi
        fi
    fi
}

if [[ ${1:-} = "--fix" ]]; then
    shift

    # Try to fix issues in ALL Python files via autopep8 and isort

    cki_echo_yellow "Running autopep8"
    if ! autopep8 --max-line-length "${MAX_LINE_LENGTH}" --exclude "migrations" --in-place --recursive .; then
        FAILED+=(autopep8)
    fi

    maybe_run_linter isort "${disabled_linters[*]}"
else
    # Check ALL Python files with flake8 (which includes pycodestyle), pydocstyle and isort

    read -ra pylint_args <<< "${CKI_PYLINT_ARGS:-}"
    read -ra pytest_args <<< "${CKI_PYTEST_ARGS:-}"
    read -ra pytest_ignorelist <<< "${CKI_PYTEST_IGNORELIST:-inttests/}"
    pytest_args+=(
        --junitxml=coverage/junit.xml
        "${pytest_ignorelist[@]/#/--ignore=}"
        --color=yes
        --verbose
        -r s
    )

    maybe_run_linter markdownlint "${disabled_linters[*]}"
    maybe_run_linter flake8 "${disabled_linters[*]}"
    maybe_run_linter pydocstyle "${disabled_linters[*]}"
    maybe_run_linter isort "${disabled_linters[*]}" --check --diff
    maybe_run_linter mypy "${disabled_linters[*]}"
    maybe_run_linter pylint "${disabled_linters[*]}" "${pylint_args[@]}" "$@"

    run_pytest "$@"
fi

if (( ${#FAILED[@]} > 0 )); then
    cki_echo_red "Failed linting steps: ${FAILED[*]}"
    exit 1
fi
