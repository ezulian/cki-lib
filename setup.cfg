[metadata]
name = cki-lib
description = Random pieces of code used by the CKI team.
long_description = file: README.md
version = 0.0.1
author = CKI Team
author_email = cki-project@redhat.com
license = GPLv3

[options]
# Automatically find all packages
packages = find:
# Parse the MANIFEST.in file and include those files, too.
include_package_data = True
install_requires =
    urllib3
    boto3
    cached-property
    crontab
    furl
    gql
    jsonschema[format]
    kcidb-io @ git+https://gitlab.com/cki-project/mirror/kcidb-io.git@production
    kubernetes
    pika
    prometheus-client
    python-dateutil
    python-gitlab
    PyYAML
    requests
    sentry-sdk
scripts =
    shell-scripts/cki_check_links.sh
    shell-scripts/cki_entrypoint.sh
    shell-scripts/cki_lint.sh
    shell-scripts/cki_utils.sh

[options.entry_points]
console_scripts =
    cki_is_production = cki_lib.misc:_is_production_cli
    cki_is_staging = cki_lib.misc:_is_staging_cli
    cki_deployment_environment = cki_lib.misc:_deployment_environment_cli

[options.extras_require]
# kcidb should be pinned to version compatible with kcidb-io,
# it's currently not used directly by cki-lib
kcidb =
    kcidb @ git+https://gitlab.com/cki-project/mirror/kcidb.git@production
crypto =
    cryptography
dev =
    freezegun
    # responses==0.23.2 depends on urllib3>=2, which we'll be avoiding
    # until we are sure all our dependencies (e.g. botocore) can support it
    responses<0.23.2
    types-PyYAML
    types-requests
psql =
    psycopg2
teiid =
    twisted
umb =
    stomp.py

[options.packages.find]
# Don't include the /tests directory when we search for python files.
exclude =
    tests*

[tox:tox]
envlist = full

[flake8]
per-file-ignores =
    cki_lib/kcidb/__init__.py:F401

[testenv]
passenv =
    RABBITMQ_HOST
    STOMP_HOST
    STOMP_PORT
    STOMP_CERTFILE
commands = cki_lint.sh cki_lib

[testenv:full]
extras =
    kcidb
    crypto
    dev
    psql
    teiid
    umb

[testenv:pipeline]
extras =
    dev
set_env =
    CKI_COVERAGE_ENABLED=false
    CKI_DISABLED_LINTERS=all
    CKI_PYLINT_ARGS=--disable E0401
install_command = python -I -m pip install --constraint constraints.txt {opts} {packages}
