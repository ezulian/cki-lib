"""Stomp message sending."""
import contextlib
import json
import os
import typing

import stomp

from . import certs
from . import logger
from . import misc

LOGGER = logger.get_logger(__name__)


class StompClient:
    """Class for handling UMB communication."""

    def __init__(
        self,
        *,
        host: typing.Union[None, str, list[str]] = None,
        port: typing.Optional[int] = None,
        certfile: typing.Optional[str] = None,
    ) -> None:
        """Initialize a client."""
        if not host:
            host = os.environ.get('STOMP_HOST', 'localhost')
        if isinstance(host, str):
            host = host.split()
        port = int(port or misc.get_env_int('STOMP_PORT', 61612))

        self.brokers = [(h, port) for h in host]
        self.certfile = certfile or os.environ.get('STOMP_CERTFILE')

    @contextlib.contextmanager
    def connect(self) -> stomp.Connection:
        """Connect to the server and yield a connection."""
        connection = stomp.Connection(self.brokers, keepalive=True)
        if self.certfile:
            connection.set_ssl(self.brokers,
                               key_file=self.certfile,
                               cert_file=self.certfile)
            certs.update_certificate_metrics(self.certfile)
        connection.connect(wait=True)
        try:
            yield connection
        finally:
            with contextlib.suppress(Exception):
                connection.disconnect()

    def send_message(self, data: typing.Any, queue_name: str) -> None:
        """Send message to topic.

        Encode `data` as json and send it to topic=queue_name.
        """
        logging_env = {
            'send_message': {
                'body': data,
                'routing_key': queue_name,
            }
        }
        with logger.logging_env(logging_env):
            LOGGER.info('Sending message with routing_key=%s', queue_name)
            with self.connect() as connection:
                connection.send(queue_name, json.dumps(data))
